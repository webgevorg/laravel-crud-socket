<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscribeController extends Controller
{
    /**
     * Subscribe to products
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function articles(Request $request)
    {
        auth()->user()->subscribe_articles = $request->get('on', 0);
        auth()->user()->save();

        return redirect()->back();
    }

    /**
     * Subscribe to articles
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function products(Request $request)
    {
        auth()->user()->subscribe_products = $request->get('on', 0);
        auth()->user()->save();

        return redirect()->back();
    }
}
