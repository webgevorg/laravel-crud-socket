<?php

namespace App\Observers;

use App\Events\NewProductAdded;
use App\Models\Product;

class ProductObserver
{
    /**
     * Handle the Product "created" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        event(new NewProductAdded);
    }
}
