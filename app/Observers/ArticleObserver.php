<?php

namespace App\Observers;

use App\Events\NewArticleAdded;
use App\Models\Article;

class ArticleObserver
{
    /**
     * Handle the Article "created" event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function created(Article $article)
    {
        event(new NewArticleAdded);
    }
}
