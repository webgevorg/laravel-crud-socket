<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
    ];

    /**
     * Get formatted price attribute
     */
    public function getFormattedPriceAttribute()
    {
        return '$ ' . $this->price;
    }

    /**
     * Product subscriptions
     * @return MorphMany
     */
    public function subscribes(): MorphMany
    {
        return $this->morphMany(Subscribe::class, 'subscribeable');
    }
}
