<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('subscribe/articles', [App\Http\Controllers\SubscribeController::class, 'articles'])
        ->name('subscribe.articles');

    Route::get('subscribe/products', [App\Http\Controllers\SubscribeController::class, 'products'])
        ->name('subscribe.products');

    Route::resource('users', App\Http\Controllers\UserController::class);
    Route::resource('products', App\Http\Controllers\ProductController::class);
    Route::resource('articles', App\Http\Controllers\ArticleController::class);
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
