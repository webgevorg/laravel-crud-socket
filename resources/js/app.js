require('./bootstrap');

import Vue from 'vue'
import Subscribe from "./Subscribe";

new Vue({
    el: '#app',
    components: {
        Subscribe,
    }
});
