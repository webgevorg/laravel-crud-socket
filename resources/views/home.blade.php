@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(auth()->check() && auth()->user()->is_partner)
                         <a class="btn btn-primary" href="{{ route('users.index') }}">{{ __('Manage users') }}</a>
                         <a class="btn btn-primary" href="{{ route('articles.index') }}">{{ __('Manage articles') }}</a>
                         <a class="btn btn-primary" href="{{ route('products.index') }}">{{ __('Manage products') }}</a>
                    @else
                        @if(auth()->check() && ! auth()->user()->subscribe_products)
                            <a class="btn btn-primary" href="{{ route('subscribe.products') }}?on=1">{{ __('Subscribe products') }}</a>
                        @else
                            <a class="btn btn-danger" href="{{ route('subscribe.products') }}?on=0">{{ __('Unsubscribe products') }}</a>
                        @endif

                        @if(auth()->check() && ! auth()->user()->subscribe_articles)
                            <a class="btn btn-primary" href="{{ route('subscribe.articles') }}?on=1">{{ __('Subscribe articles') }}</a>
                        @else
                            <a class="btn btn-danger" href="{{ route('subscribe.articles') }}?on=0">{{ __('Unsubscribe articles') }}</a>
                        @endif
                    @endif
                </div>
            </div>
            @include('products.list')
            @include('articles.list')
        </div>

    </div>
</div>
@endsection
