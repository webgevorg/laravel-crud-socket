<div class="row justify-content-center">
    <div class="col-12 pt-4 pb-4 d-flex justify-content-between">
        <h4>{{ __('Users') }}</h4>
        @if(route('users.index') === request()->url())
            <div>
                <a class="btn btn-success" href="{{ route('users.create') }}">{{ __('Add new User') }}</a>
            </div>
        @endif
    </div>
</div>
