<h2 class="mt-4">Last 5 Products</h2>

@forelse($products as $product)
    <div class="card mt-2">
        <div class="card-body">
            <h5 class="card-title">{{ $product->name }}</h5>
            <p class="card-text">{{ $product->formatted_price }}</p>
        </div>
    </div>
@empty
    {{ __('Nothing found') }}
@endforelse
