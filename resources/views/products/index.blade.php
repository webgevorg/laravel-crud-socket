@extends('layouts.app')

@section('content')
    <div class="container">
        @include('products.partials.head')
        <div class="row justify-content-center">
            <div class="col-12">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">{{ __('Name') }}</th>
                        <th scope="col">{{ __('Price') }}</th>
                        <th scope="col">{{ __('Created At') }}</th>
                        <th scope="col">{{ __('Actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <th scope="row">{{ $product->id }}</th>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->formatted_price }}</td>
                            <td>{{ $product->created_at }}</td>
                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-warning mr-2" href="{{ route('products.edit', $product) }}">{{ __('Edit') }}</a>
                                    <form action="{{ route('products.destroy', $product) }}" method="post">
                                        @csrf
                                        {{ method_field('delete') }}
                                        <button class="btn btn-danger" type="submit">{{ __('Delete') }}</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{ $products->links() }}
            </div>
        </div>
    </div>
@endsection
