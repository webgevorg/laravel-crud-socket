<div class="row justify-content-center">
    <div class="col-12 pt-4 pb-4 d-flex justify-content-between">
        <h4>{{ __('Products') }}</h4>
        @if(route('products.index') === request()->url())
            <div>
                <a class="btn btn-success" href="{{ route('products.create') }}">{{ __('Add new Product') }}</a>
            </div>
        @endif
    </div>
</div>
