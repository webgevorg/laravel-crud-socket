@extends('layouts.app')

@section('content')
    <div class="container">
        @include('articles.partials.head')
        <div class="row justify-content-center">
            <div class="col-12">
                <form action="{{ route('articles.update', $article) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('put') }}

                    <div class="form-group row">
                        <label for="title" class="col-12 col-form-label">{{ __('Title') }}</label>

                        <div class="col-12">
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') ?? $article->title }}">

                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="content" class="col-12 col-form-label">{{ __('Content') }}</label>

                        <div class="col-12">
                            <textarea id="content" type="text" class="form-control @error('content') is-invalid @enderror" name="content">{{ old('content') ?? $article->content }}</textarea>

                            @error('content')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
