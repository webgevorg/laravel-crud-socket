<h2 class="mt-4">Last 5 Articles</h2>

@forelse($articles as $article)
    <div class="card mt-2">
        <div class="card-body">
            <h5 class="card-title">{{ $article->title }}</h5>
            <p class="card-text">{{ $article->content }}</p>
        </div>
    </div>
@empty
    {{ __('Nothing found') }}
@endforelse
