@extends('layouts.app')

@section('content')
    <div class="container">
        @include('articles.partials.head')
        <div class="row justify-content-center">
            <div class="col-12">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">{{ __('Title') }}</th>
                        <th scope="col">{{ __('Content') }}</th>
                        <th scope="col">{{ __('Created At') }}</th>
                        <th scope="col">{{ __('Actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articles as $article)
                        <tr>
                            <th scope="row">{{ $article->id }}</th>
                            <td>{{ $article->title }}</td>
                            <td>{{ $article->content }}</td>
                            <td>{{ $article->created_at }}</td>
                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-warning mr-2" href="{{ route('articles.edit', $article) }}">{{ __('Edit') }}</a>
                                    <form action="{{ route('products.destroy', $article) }}" method="post">
                                        @csrf
                                        {{ method_field('delete') }}
                                        <button class="btn btn-danger" type="submit">{{ __('Delete') }}</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{ $articles->links() }}
            </div>
        </div>
    </div>
@endsection
