<div class="row justify-content-center">
    <div class="col-12 pt-4 pb-4 d-flex justify-content-between">
        <h4>{{ __('Articles') }}</h4>
        @if(route('articles.index') === request()->url())
            <div>
                <a class="btn btn-success" href="{{ route('articles.create') }}">{{ __('Add new Article') }}</a>
            </div>
        @endif
    </div>
</div>
